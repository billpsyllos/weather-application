# weatherApp
WeatherApp responsibility is to provide daily weather reports

# Design
Actually contains 3 main modules: 

-weather-api: which contains the useCases in our case to retrieve daily weather summary and the related dtos


-weather-services: which contains the client which execute the above use case, in our case is a scheduler


-wehather-provider: which contains the implementation of the use case

# notes
Because of some gradlew issues in order to run the application please do it directly from the WeatherConfiguration.class

In additional regarding the above conflicts for some reason my weather-provider module could not detect its properties, consequently some properties are hardcoded 

*Find also a draft of the exported xml file in to daily summary folder