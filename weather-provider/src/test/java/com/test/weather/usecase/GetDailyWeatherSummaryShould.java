package com.test.weather.usecase;

import com.test.weather.converter.WeatherSummaryRequestConverter;
import com.test.weather.converter.WeatherSummaryResponseConverter;
import com.test.weather.external.model.DailySummary;
import com.test.weather.external.model.ExternalWeatherSummaryResponse;
import com.test.weather.external.model.History;
import com.test.weather.external.usecase.GetWeatherSummaryFromExternalProviderUseCase;
import com.test.weather.model.WeatherSummary;
import com.test.weather.model.WeatherSummaryRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GetDailyWeatherSummaryShould {

    @Mock
    private GetWeatherSummaryFromExternalProviderUseCase getWeatherSummaryFromExternalProviderUseCase;
    private GetDailyWeatherSummary getDailyWeatherSummary;
    private WeatherSummaryRequestConverter weatherSummaryRequestConverter;
    private WeatherSummaryResponseConverter weatherSummaryResponseConverter;

    @BeforeEach
    public void setUp(){
        initMocks(this);
        ExternalWeatherSummaryResponse externalWeatherResponse = createExternalWeatherResponse();
        when(getWeatherSummaryFromExternalProviderUseCase.execute(Mockito.any())).thenReturn(externalWeatherResponse);


        weatherSummaryRequestConverter = new WeatherSummaryRequestConverter();
        weatherSummaryResponseConverter = new WeatherSummaryResponseConverter();

        getDailyWeatherSummary = new GetDailyWeatherSummary(
                weatherSummaryRequestConverter,
                getWeatherSummaryFromExternalProviderUseCase,
                weatherSummaryResponseConverter
        );
    }

    public static ExternalWeatherSummaryResponse createExternalWeatherResponse() {
        ExternalWeatherSummaryResponse externalWeatherSummaryResponse = new ExternalWeatherSummaryResponse();
        externalWeatherSummaryResponse.setHistory(setHistory());

        return externalWeatherSummaryResponse;
    }

    private static History setHistory() {
        History history = new History();
        history.setDailysummary(createDailySummary());

        return history;
    }

    private static List<DailySummary> createDailySummary() {
        DailySummary dailySummary = new DailySummary();

        dailySummary.setMaxhumidity("34");
        dailySummary.setMaxtempm("12");
        dailySummary.setMintempm("1");
        dailySummary.setPrecipm("12");

        return List.of(dailySummary);
    }

    @Test
    public void returnTheWeatherSummary(){
        WeatherSummary weatherSummary = new WeatherSummary();
        WeatherSummary expectedWeatherSummary = new WeatherSummary();
        WeatherSummaryRequest weatherSummaryRequest = new WeatherSummaryRequest(LocalDate.now().toString(), "New_York");

        getDailyWeatherSummary.execute(weatherSummaryRequest);

        assertThat(weatherSummary.getMaxhumidity()).isEqualTo(expectedWeatherSummary.getMaxhumidity());
    }
}
