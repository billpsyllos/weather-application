package com.test.weather.usecase;

import com.test.weather.external.exceptions.GetWeatherFromExternalProviderException;
import com.test.weather.external.model.DailySummary;
import com.test.weather.external.model.ExternalWeatherSummaryRequest;
import com.test.weather.external.model.ExternalWeatherSummaryResponse;
import com.test.weather.external.usecase.GetWeatherFromExternalProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GetWeatherSummaryFromExternalProviderShould {

    @Mock
    private RestTemplate externalProviderHttpClient;
    private GetWeatherFromExternalProvider getWeatherFromExternalProvider;

    @BeforeEach
    public void setUp(){
        initMocks(this);
        getWeatherFromExternalProvider = new GetWeatherFromExternalProvider(
                externalProviderHttpClient
        );
    }

    private void mockRestTemplateSuccessBehavior(){
        when(externalProviderHttpClient.exchange(
                Mockito.anyString(),
                Mockito.anyObject(),
                Mockito.anyObject(),
                (ParameterizedTypeReference<Object>) Mockito.anyObject()
                )
        ).thenReturn(ResponseEntity.ok().body(GetDailyWeatherSummaryShould.createExternalWeatherResponse()));
    }

    @Test
    public void returnWeatherSummary(){
        mockRestTemplateSuccessBehavior();

        ExternalWeatherSummaryRequest externalWeatherSummaryRequest = getExternalWeatherSummaryRequest();

        ExternalWeatherSummaryResponse externalWeatherSummaryResponse = getWeatherFromExternalProvider.execute(externalWeatherSummaryRequest);
        List<DailySummary> dailySummary =  externalWeatherSummaryResponse.getHistory().getDailysummary();

        assertThat(dailySummary.get(0).getMaxhumidity()).isEqualTo("34");
        assertThat(dailySummary.get(0).getMaxtempm()).isEqualTo("12");
        assertThat(dailySummary.get(0).getMintempm()).isEqualTo("1");
        assertThat(dailySummary.get(0).getPrecipm()).isEqualTo("12");
    }

    private ExternalWeatherSummaryRequest getExternalWeatherSummaryRequest() {
        ExternalWeatherSummaryRequest externalWeatherSummaryRequest = new ExternalWeatherSummaryRequest();
        externalWeatherSummaryRequest.setDate("20180418");
        externalWeatherSummaryRequest.setCity("New_York");
        return externalWeatherSummaryRequest;
    }

    @Nested
    @DisplayName("When provider request fails")
    class whenSearchQueryValidationFails {

        private void mockRestTemplateFailureBehavior() throws GetWeatherFromExternalProviderException {
            doThrow(new GetWeatherFromExternalProviderException("foo", GetWeatherFromExternalProviderException.Type.GENERIC))
                    .when(externalProviderHttpClient)
                    .exchange(
                            Mockito.anyString(),
                            Mockito.any(),
                            Mockito.any(),
                            (ParameterizedTypeReference<Object>) Mockito.any()
                    );
        }

        @Test
        public void propagateExternalProviderException() {
            mockRestTemplateFailureBehavior();
            Executable executeUseCase = () -> getWeatherFromExternalProvider.execute(getExternalWeatherSummaryRequest());

            assertThrows(GetWeatherFromExternalProviderException.class, executeUseCase);
        }
    }
}
