package com.test.weather.external.usecase;

import com.test.weather.external.model.ExternalWeatherSummaryRequest;
import com.test.weather.external.model.ExternalWeatherSummaryResponse;

public interface GetWeatherSummaryFromExternalProviderUseCase {
    ExternalWeatherSummaryResponse execute(ExternalWeatherSummaryRequest externalWeatherSummaryRequest);
}
