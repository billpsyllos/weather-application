package com.test.weather.external.model;

public class ExternalWeatherSummaryResponse {
    private History history;

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }
}
