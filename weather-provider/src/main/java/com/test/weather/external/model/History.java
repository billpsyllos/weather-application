package com.test.weather.external.model;

import java.util.Collections;
import java.util.List;

public class History {
    private List<DailySummary> dailysummary = Collections.EMPTY_LIST;

    public List<DailySummary> getDailysummary() {
        return dailysummary;
    }

    public void setDailysummary(List<DailySummary> dailysummary) {
        this.dailysummary = dailysummary;
    }
}
