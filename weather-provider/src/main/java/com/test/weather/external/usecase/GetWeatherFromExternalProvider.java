package com.test.weather.external.usecase;

import com.test.weather.external.exceptions.GetWeatherFromExternalProviderException;
import com.test.weather.external.model.ExternalWeatherSummaryRequest;
import com.test.weather.external.model.ExternalWeatherSummaryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;

@Component
public class GetWeatherFromExternalProvider implements GetWeatherSummaryFromExternalProviderUseCase {
    private static final Logger LOG = LoggerFactory.getLogger(GetWeatherFromExternalProvider.class);

    private final RestTemplate externalProviderHttpClient;

    @Autowired
    public GetWeatherFromExternalProvider(RestTemplate externalProviderHttpClient) {
        this.externalProviderHttpClient = externalProviderHttpClient;
    }

    @Override
    public ExternalWeatherSummaryResponse execute(ExternalWeatherSummaryRequest externalWeatherSummaryRequest) {
        String url = createUrl();
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        Map<String, String> map = new HashMap<>();
        headers.setAll(map);

        try {
            LOG.info("Retrieving the weather daily summary from external service");
            ResponseEntity< ExternalWeatherSummaryResponse > response = externalProviderHttpClient.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference< ExternalWeatherSummaryResponse >() {});

            return response.getBody();

        } catch (HttpStatusCodeException ex) {
            throw new GetWeatherFromExternalProviderException("Generic Error", GetWeatherFromExternalProviderException.Type.GENERIC);
        }

    }

    private String createUrl() {
        return "http://api.wunderground.com/api/26ceba2ee6b2236c/history_20180416/q/NY/New_York.json";
    }
}
