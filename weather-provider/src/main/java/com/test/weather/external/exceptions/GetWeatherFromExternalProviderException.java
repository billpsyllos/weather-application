package com.test.weather.external.exceptions;

public class GetWeatherFromExternalProviderException extends RuntimeException {
    private final Type type;

    public enum Type {
        GENERIC
    }

    public GetWeatherFromExternalProviderException(String message, Type type) {
        super(message);
        this.type = type;
    }

    public GetWeatherFromExternalProviderException(String message, Throwable cause, Type type) {
        super(message, cause);
        this.type = type;
    }

    public GetWeatherFromExternalProviderException(Throwable cause, Type type) {
        super(cause);
        this.type = type;
    }

    public GetWeatherFromExternalProviderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Type type) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
