package com.test.weather.usecase;

import com.test.weather.converter.WeatherSummaryRequestConverter;
import com.test.weather.converter.WeatherSummaryResponseConverter;
import com.test.weather.external.model.ExternalWeatherSummaryRequest;
import com.test.weather.external.model.ExternalWeatherSummaryResponse;
import com.test.weather.external.usecase.GetWeatherSummaryFromExternalProviderUseCase;
import com.test.weather.model.WeatherSummary;
import com.test.weather.model.WeatherSummaryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetDailyWeatherSummary implements GetDailyWeatherSummaryUseCase{

    private final WeatherSummaryRequestConverter weatherSummaryRequestConverter;
    private final GetWeatherSummaryFromExternalProviderUseCase getWeatherSummaryFromExternalProviderUseCase;
    private final WeatherSummaryResponseConverter weatherSummaryResponseConverter;

    @Autowired
    public GetDailyWeatherSummary(WeatherSummaryRequestConverter weatherSummaryRequestConverter, GetWeatherSummaryFromExternalProviderUseCase getWeatherSummaryFromExternalProviderUseCase, WeatherSummaryResponseConverter weatherSummaryResponseConverter) {
        this.weatherSummaryRequestConverter = weatherSummaryRequestConverter;
        this.getWeatherSummaryFromExternalProviderUseCase = getWeatherSummaryFromExternalProviderUseCase;
        this.weatherSummaryResponseConverter = weatherSummaryResponseConverter;
    }

    @Override
    public WeatherSummary execute(WeatherSummaryRequest weatherSummaryRequest) {
        ExternalWeatherSummaryRequest externalWeatherSummaryRequest = weatherSummaryRequestConverter.createRequest(weatherSummaryRequest);

        ExternalWeatherSummaryResponse externalWeatherSummaryResponse = getWeatherSummaryFromExternalProviderUseCase.execute(externalWeatherSummaryRequest);

        return weatherSummaryResponseConverter.convert(externalWeatherSummaryResponse);
    }
}
