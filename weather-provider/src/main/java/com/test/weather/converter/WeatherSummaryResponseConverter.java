package com.test.weather.converter;

import com.test.weather.external.model.DailySummary;
import com.test.weather.external.model.ExternalWeatherSummaryResponse;
import com.test.weather.model.WeatherSummary;
import org.springframework.stereotype.Component;

@Component
public class WeatherSummaryResponseConverter {

    public WeatherSummary convert(ExternalWeatherSummaryResponse externalWeatherSummaryResponse){
        DailySummary dailySummary = externalWeatherSummaryResponse.getHistory().getDailysummary().get(0);

        WeatherSummary weatherSummary = new WeatherSummary();
        weatherSummary.setMaxhumidity(dailySummary.getMaxhumidity());
        weatherSummary.setMaxtempm(dailySummary.getMaxtempm());
        weatherSummary.setMintempm(dailySummary.getMintempm());
        weatherSummary.setPrecipm(dailySummary.getPrecipm());

        return weatherSummary;
    }
}
