package com.test.weather.converter;

import com.test.weather.external.model.ExternalWeatherSummaryRequest;
import com.test.weather.model.WeatherSummaryRequest;
import org.springframework.stereotype.Component;

@Component
public class WeatherSummaryRequestConverter {
    public ExternalWeatherSummaryRequest createRequest(WeatherSummaryRequest weatherSummaryRequest) {
        ExternalWeatherSummaryRequest externalWeatherSummaryRequest = new ExternalWeatherSummaryRequest();
        externalWeatherSummaryRequest.setCity(weatherSummaryRequest.getCity());
        externalWeatherSummaryRequest.setDate(weatherSummaryRequest.getDate());

        return externalWeatherSummaryRequest;
    }
}
