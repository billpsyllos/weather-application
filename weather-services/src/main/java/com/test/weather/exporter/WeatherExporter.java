package com.test.weather.exporter;

import com.test.weather.model.WeatherSummary;
import org.springframework.stereotype.Component;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

@Component
public class WeatherExporter {

    public static final String METRICS = "metrics";
    public static final String TYPE = "type";
    public static final String ROOT_ELEMENT = "weather";
    public static final String SUMMARY_ELEMENT = "summary";
    public static final String CITY = "city";
    public static final String CITY_NAME = "New York";

    public void exportToXml(WeatherSummary weatherSummary){
        try {
            DocumentBuilderFactory dbFactory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            // root element
            Element rootElement = doc.createElement(ROOT_ELEMENT);
            doc.appendChild(rootElement);

            // summary element
            Element summary = doc.createElement(SUMMARY_ELEMENT);
            rootElement.appendChild(summary);

            // setting attribute to element
            Attr attr = doc.createAttribute(CITY);
            attr.setValue(CITY_NAME);
            summary.setAttributeNode(attr);

            // metrics elements
            Element metrics = doc.createElement(METRICS);
            Attr attrType = doc.createAttribute(TYPE);
            attrType.setValue("maxhumidity");
            metrics.setAttributeNode(attrType);
            metrics.appendChild(doc.createTextNode(weatherSummary.getMaxhumidity()));
            summary.appendChild(metrics);

            Element metrics1 = doc.createElement(METRICS);
            Attr attrType1 = doc.createAttribute(TYPE);
            attrType1.setValue("maxtempm");
            metrics1.setAttributeNode(attrType1);
            metrics1.appendChild(doc.createTextNode(weatherSummary.getMaxtempm()));
            summary.appendChild(metrics1);

            Element metrics2 = doc.createElement(METRICS);
            Attr attrType2 = doc.createAttribute(TYPE);
            attrType2.setValue("mintempm");
            metrics2.setAttributeNode(attrType2);
            metrics2.appendChild(doc.createTextNode(weatherSummary.getMintempm()));
            summary.appendChild(metrics2);

            Element metrics3 = doc.createElement(METRICS);
            Attr attrType3 = doc.createAttribute(TYPE);
            attrType3.setValue("precipm");
            metrics3.setAttributeNode(attrType3);
            metrics3.appendChild(doc.createTextNode(weatherSummary.getPrecipm()));
            summary.appendChild(metrics3);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);

            String exporterdPath = WeatherExporter.class.getProtectionDomain().getCodeSource().getLocation().getPath();

            StreamResult result = new StreamResult(new File(exporterdPath + "/weather-daily-summary.xml"));
            transformer.transform(source, result);

            // Output to console for testing
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
