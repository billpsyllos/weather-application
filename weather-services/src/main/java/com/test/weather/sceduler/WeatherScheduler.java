package com.test.weather.sceduler;

import com.test.weather.exporter.WeatherExporter;
import com.test.weather.model.WeatherSummary;
import com.test.weather.model.WeatherSummaryRequest;
import com.test.weather.usecase.GetDailyWeatherSummaryUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class WeatherScheduler {
    private static final Logger LOG = LoggerFactory.getLogger(WeatherScheduler.class);

    public static final String DATE_FORMAT = "yyyymmdd";
    public static final String CITY = "New_York";

    private final GetDailyWeatherSummaryUseCase getDailyWeatherSummaryUseCase;
    private final WeatherExporter weatherExporter;

    @Autowired
    public WeatherScheduler(GetDailyWeatherSummaryUseCase getDailyWeatherSummaryUseCase, WeatherExporter weatherExporter) {
        this.getDailyWeatherSummaryUseCase = getDailyWeatherSummaryUseCase;
        this.weatherExporter = weatherExporter;
    }

    @Scheduled(fixedRate = 1000000)
    public void getDailyWeather() {
        WeatherSummary weatherSummary = getDailyWeatherSummaryUseCase.execute(createWeatherRequest());
        weatherExporter.exportToXml(weatherSummary);

        LOG.info("Retrieved and exported weather daily summary successfully!!");
    }

    private WeatherSummaryRequest createWeatherRequest() {
        return new WeatherSummaryRequest(getCurrentDate(), CITY);
    }

    private String getCurrentDate() {
        LocalDateTime now = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

        String formatDateTime = now.format(formatter);

        return formatDateTime.toString();
    }
}
