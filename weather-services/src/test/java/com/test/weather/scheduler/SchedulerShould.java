package com.test.weather.scheduler;

import com.test.weather.exporter.WeatherExporter;
import com.test.weather.model.WeatherSummary;
import com.test.weather.sceduler.WeatherScheduler;
import com.test.weather.usecase.GetDailyWeatherSummaryUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class SchedulerShould {

    @Mock
    private GetDailyWeatherSummaryUseCase getDailyWeatherSummaryUseCase;
    @Mock
    private WeatherExporter weatherExporter;
    private WeatherScheduler weatherScheduler;

    @BeforeEach
    public void setUp(){
        initMocks(this);
        weatherScheduler = new WeatherScheduler(getDailyWeatherSummaryUseCase, weatherExporter);
    }

    @Test
    public void triggerTheGetDailyWeatherSummaryUseCase() {
        weatherScheduler.getDailyWeather();

        verify(getDailyWeatherSummaryUseCase).execute(Mockito.any());
    }
}
