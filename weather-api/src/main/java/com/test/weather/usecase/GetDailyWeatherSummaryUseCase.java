package com.test.weather.usecase;

import com.test.weather.model.WeatherSummary;
import com.test.weather.model.WeatherSummaryRequest;

public interface GetDailyWeatherSummaryUseCase {
    WeatherSummary execute(WeatherSummaryRequest weatherSummaryRequest);
}
