package com.test.weather.model;

public class WeatherSummary {
    private String maxhumidity;
    private String maxtempm;
    private String precipm;
    private String mintempm;

    public String getMaxhumidity() {
        return maxhumidity;
    }

    public void setMaxhumidity(String maxhumidity) {
        this.maxhumidity = maxhumidity;
    }

    public String getMaxtempm() {
        return maxtempm;
    }

    public void setMaxtempm(String maxtempm) {
        this.maxtempm = maxtempm;
    }

    public String getPrecipm() {
        return precipm;
    }

    public void setPrecipm(String precipm) {
        this.precipm = precipm;
    }

    public String getMintempm() {
        return mintempm;
    }

    public void setMintempm(String mintempm) {
        this.mintempm = mintempm;
    }
}
