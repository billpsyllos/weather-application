package com.test.weather.model;

import java.time.LocalDate;

public class WeatherSummaryRequest {
    private String date;
    private String city;

    public WeatherSummaryRequest(String date, String city) {
        this.date = date;
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
